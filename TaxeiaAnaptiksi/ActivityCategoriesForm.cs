﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxeiaAnaptiksi
{
    public partial class ActivityCategoriesForm : Form
    {
        Login userLogin = new Login();
        public ActivityCategoriesForm()
        {
            InitializeComponent();
            userLogin.activitiesForm = true;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                ScubaDivingForm f4 = new ScubaDivingForm();
                f4.ShowDialog();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           if  (checkIfUserInDb())
            {
                CaveForm f5 = new CaveForm();
                f5.ShowDialog();
            }
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                MonumentsForm f6 = new MonumentsForm();
                f6.ShowDialog();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // ama akouei kati, ftiaxnei analogws tis eikones gia thn mousikh
            if (SoundClass.doYouHearMusic)
            {
                pictureBox6.Visible = true;
                pictureBox5.Visible = false;
            }
            else
            {
                pictureBox6.Visible = false;
                pictureBox5.Visible = true;
            }
        }

        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                BeachesForm f7 = new BeachesForm();
                f7.ShowDialog();
            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox6.Visible = false;
            pictureBox5.Visible = true;
            
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox6.Visible = true;
            pictureBox5.Visible = false;
        }

        private void ActivityCategoriesForm_Activated(object sender, EventArgs e)
        {
            // ama akouei kati, ftiaxnei analogws tis eikones gia thn mousikh
            if (SoundClass.doYouHearMusic)
            {
                pictureBox6.Visible = true;
                pictureBox5.Visible = false;
            }
            else
            {
                pictureBox6.Visible = false;
                pictureBox5.Visible = true;
            }
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
        private bool checkIfUserInDb()
        {
            //SOS metakinhse to return true apo katw gia otan leitourgiko.
            //return true;
            if (Validations.isUserLoggedIn == false)
            {
                MessageBox.Show("Πρέπει να είστε εγγεγραμμένος για να έχετε πρόσβαση σε αυτή τη φόρμα.");
                RegisterForm f8 = new RegisterForm();
                f8.ShowDialog();
                return false;
            }
            return true;
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
