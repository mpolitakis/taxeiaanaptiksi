﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TaxeiaAnaptiksi
{
    public partial class BeachesForm : Form
    {
        int counter;
        public BeachesForm()
        {
            InitializeComponent();
        }

        private void BeachesForm_Load(object sender, EventArgs e)
        {
            if (SoundClass.doYouHearMusic)
            {
                pictureBox5.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
            timer1.Enabled=true;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox5.Visible = false;
            pictureBox4.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox5.Visible = true;
            pictureBox4.Visible = false;
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter++;
            if (counter%4==0)
            {
                antisamos_beach.Visible = false;
                antisamos_label.Visible = false;
                myrtos_beach.Visible = false;
                myrtos_label.Visible = false;
                skala_beach.Location = new Point(12, 114);
                skala_label.Location = new Point(84, 78);
                xi_beach.Location = new Point(630, 114);
                xi_label.Location = new Point(757, 78);
                skala_beach.Visible = true;
                skala_label.Visible = true;
                xi_beach.Visible = true;
                xi_label.Visible = true;
            }
            else if (counter%3==0)
            {
                antisamos_beach.Visible = false;
                antisamos_label.Visible = false;
                skala_beach.Visible = false;
                skala_label.Visible = false;
                xi_beach.Location = new Point(12, 114);
                xi_label.Location = new Point(84, 78);
                myrtos_beach.Location = new Point(630, 114);
                myrtos_label.Location = new Point(757, 78);
                xi_beach.Visible = true;
                xi_label.Visible = true;
                myrtos_beach.Visible = true;
                myrtos_label.Visible = true;
            }
            else if (counter%2==0)
            {
                skala_beach.Visible = false;
                skala_label.Visible = false;
                xi_beach.Visible = false;
                xi_label.Visible = false;
                antisamos_beach.Location = new Point(630,114);
                antisamos_label.Location = new Point(757,78);
                myrtos_beach.Location = new Point(12, 114);
                myrtos_label.Location = new Point(84, 78);
                antisamos_beach.Visible = true;
                antisamos_label.Visible = true;
                myrtos_beach.Visible = true;
                myrtos_label.Visible = true;
            }
            else
            {
                myrtos_beach.Visible = false;
                myrtos_label.Visible = false;
                xi_beach.Visible = false;
                xi_label.Visible = false;
                antisamos_beach.Location = new Point(12, 114);
                antisamos_label.Location = new Point(84, 78);
                skala_beach.Location = new Point(630, 114); 
                skala_label.Location = new Point(757, 78);
                skala_beach.Visible = true;
                skala_label.Visible = true;
                antisamos_beach.Visible = true;
                antisamos_label.Visible = true;
            }
            
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
