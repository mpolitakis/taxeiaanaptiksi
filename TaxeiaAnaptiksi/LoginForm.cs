﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using static System.Windows.Forms.FlatButtonAppearance;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Data.OleDb;

namespace TaxeiaAnaptiksi
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        private void label5_Click(object sender, EventArgs e)
        {
            ActivityCategoriesForm f1 = new ActivityCategoriesForm();
            f1.ShowDialog();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            RegisterForm f3 = new RegisterForm();
            f3.ShowDialog();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.soundVariable.PlayLooping();
            pictureBox4.Visible = false;
            pictureBox3.Visible = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox3.Visible = true;
            pictureBox4.Visible = false;
        }

        private void LoginForm_Activated(object sender, EventArgs e)
        {
            // ama akouei kati, ftiaxnei analogws tis eikones gia thn mousikh
            if (SoundClass.doYouHearMusic)
            {
                pictureBox3.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox3.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Constants.CONNECTION_STRING;

            try
            {
                conn.Open();
                String user = usernameTextbox.Text.ToString();
                String password = passwordTextbox.Text.ToString();
              



                    OleDbCommand command = new OleDbCommand("select Count(*) from [User] where [UserName]=? and [Password]=?", conn);


                    command.Parameters.AddWithValue("@userName", user);
                    command.Parameters.AddWithValue("@password", password);
                    int result = (int)command.ExecuteScalar();

                if (result > 0)
                {
                    MessageBox.Show("Επιτυχής Σύνδεση");
                    Validations.isUserLoggedIn = true;
                    usernameTextbox.Text = "";
                    passwordTextbox.Text = "";
                }
                else
                    MessageBox.Show("Αποτυχία Σύνδεσης ξαναπροσπαθήστε");


            }
            catch (Exception ex)
            {
                MessageBox.Show("Αποτυχιά σύνδεσης λόγω" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void passwordTextbox_TextChanged(object sender, EventArgs e)
        {
            passwordTextbox.PasswordChar = '*';
        }

        private void usernameTextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void LoginForm_SizeChanged(object sender, EventArgs e)
        {

        }

        private void LoginForm_Resize(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem26_Click(object sender, EventArgs e)
        {
            RegisterForm f8 = new RegisterForm();
            f8.ShowDialog();
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            CalendarForm f1 = new CalendarForm();
            f1.ShowDialog();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.Image = Properties.Resources.kefalonia_change_picture1;
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pictureBox2.Image = Properties.Resources.kefalonia_change_picture2;
        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
