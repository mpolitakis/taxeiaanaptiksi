﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxeiaAnaptiksi
{
    public class Login
    {
        public String username { get; set; }

        public bool loginStatus = false;

        public bool monumentsForm = false;

        public bool scubaDivingForm = false;

        public bool caveForm = false;

        public bool beachesForm = false;

        public bool activitiesForm = false;
    }
}
