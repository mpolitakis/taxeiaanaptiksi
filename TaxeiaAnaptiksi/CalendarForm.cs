﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxeiaAnaptiksi
{
    public partial class CalendarForm : Form
    {
        public CalendarForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (SoundClass.doYouHearMusic)
            {
                pictureBox5.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox5.Visible = false;
            pictureBox4.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox5.Visible = true;
            pictureBox4.Visible = false;
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void label18_Click(object sender, EventArgs e)
        {
            string[] lines =
            {
                "Καταδύση στο Λάσση",
                "Κεφαλονιά 281 00",
                "Τηλεφωνό επικοινωνίας: +306940880001"
            };
            WriteFile.writeFile(lines);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            string[] lines =
           {
                "Παραλία Ξι",
                "Παραλια Ξι, Lixoúrion, Kefallinia, Greece 282 00"
            };
            WriteFile.writeFile(lines);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            string[] lines =
          {
                "Σπηλαίο Μελλισάνης",
                "Σάμη 280 80",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label9_Click(object sender, EventArgs e)
        {
            string[] lines =
        {
                "Παραλία Μυρτώς",
                "Πυλαρέοι 280 81",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label15_Click(object sender, EventArgs e)
        {
                 string[] lines =
        {
                "Κάστρο Αγίου Γεωργίου",
                "Λειβαθού 281 00",
                "Τηλεφωνό επικοινωνίας: +302671027546"
            };
            WriteFile.writeFile(lines);
        }

        private void label13_Click(object sender, EventArgs e)
        {
            string[] lines =
{
                "Κάστρο Αγίου Γεωργίου",
                "Λειβαθού 281 00",
                "Τηλεφωνό επικοινωνίας: +302671027546"
            };
            WriteFile.writeFile(lines);
        }

        private void label10_Click(object sender, EventArgs e)
        {
            string[] lines =
       {
                "Παραλία Μυρτώς",
                "Πυλαρέοι 280 81",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            string[] lines =
         {
                "Σπηλαίο Μελλισάνης",
                "Σάμη 280 80",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            string[] lines =
           {
                "Παραλία Ξι",
                "Παραλια Ξι, Lixoúrion, Kefallinia, Greece 282 00"
            };
            WriteFile.writeFile(lines);
        }

        private void label19_Click(object sender, EventArgs e)
        {
            string[] lines =
            {
                "Καταδύση στο Λάσση",
                "Κεφαλονιά 281 00",
                "Τηλεφωνό επικοινωνίας: +306940880001"
            };
            WriteFile.writeFile(lines);
        }

        private void label7_Click(object sender, EventArgs e)
        {
            string[] lines =
         {
                "Παραλία Ξι",
                "Παραλια Ξι, Lixoúrion, Kefallinia, Greece 282 00"
            };
            WriteFile.writeFile(lines);
        }

        private void label20_Click(object sender, EventArgs e)
        {
            string[] lines =
       {
                "Κατάδυση Αγία Ευφημία",
                "Κεφαλονιά 280 81",
                "Τηλεφωνό επικοινωνίας: +306940880021"

            };
            WriteFile.writeFile(lines);
            
        }

        private void label4_Click(object sender, EventArgs e)
        {
            string[] lines =
         {
                "Σπηλαίο Μελλισάνης",
                "Σάμη 280 80",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label14_Click(object sender, EventArgs e)
        {
            string[] lines =
{
                "Κάστρο Αγίου Γεωργίου",
                "Λειβαθού 281 00",
                "Τηλεφωνό επικοινωνίας: +302671027546"
            };
            WriteFile.writeFile(lines);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
         
        }

        private void label12_Click(object sender, EventArgs e)
        {
            string[] lines =
            {
                "Παραλία Μυρτώς",
                "Πυλαρέοι 280 81",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label11_Click(object sender, EventArgs e)
        {
            string[] lines =
            {
                "Παραλία Μυρτώς",
                "Πυλαρέοι 280 81",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label16_Click(object sender, EventArgs e)
        {
            string[] lines =
{
                "Κάστρο Αγίου Γεωργίου",
                "Λειβαθού 281 00",
                "Τηλεφωνό επικοινωνίας: +302671027546"
            };
            WriteFile.writeFile(lines);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            string[] lines =
         {
                "Σπηλαίο Μελλισάνης",
                "Σάμη 280 80",
                "Τηλεφωνό επικοινωνίας: +30694001265"
            };
            WriteFile.writeFile(lines);
        }

        private void label17_Click(object sender, EventArgs e)
        {
            string[] lines =
            {
                "Καταδύση στο Λάσση",
                "Κεφαλονιά 281 00",
                "Τηλεφωνό επικοινωνίας: +306940880001"
            };
            WriteFile.writeFile(lines);
        }

        private void label8_Click(object sender, EventArgs e)
        {
            string[] lines =
         {
                "Παραλία Ξι",
                "Παραλια Ξι, Lixoúrion, Kefallinia, Greece 282 00"
            };
            WriteFile.writeFile(lines);
        }

        private void toolStripMenuItem27_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}


