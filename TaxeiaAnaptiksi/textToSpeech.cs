﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace TaxeiaAnaptiksi
{
    public static class textToSpeech
    {
        private static SpeechSynthesizer _speech = new SpeechSynthesizer();

        static textToSpeech()
        {
            _speech.SelectVoice("Microsoft Stefanos");
        }
            
        public static SpeechSynthesizer speech
        {
            get { return _speech; }
        }
    }
}
