﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace TaxeiaAnaptiksi
{
    public class Validations : Login
    {
        private static bool LoggedIn=false;

        public static bool isUserLoggedIn
        {
            get { return LoggedIn; }
            set { LoggedIn = value; }
        }
        public bool ValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public bool ValidPassword(string password, string password1)
        {
            if (password == password1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
