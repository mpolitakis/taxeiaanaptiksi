﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxeiaAnaptiksi
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
            
        }
        Validations validations = new Validations();

        private void Form3_Load(object sender, EventArgs e)
        {
            this.Location=new Point(this.Location.X+40,this.Location.Y);
            // ama akouei kati, ftiaxnei analogws tis eikones
            if (SoundClass.doYouHearMusic)
            {
                pictureBox5.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // an den einai prasina kai ta dyo shmainei oti kati exei paei la8os
            if (!(textBoxEmail.BackColor==Color.Green&&textBoxPassword.BackColor==Color.Green))
            {
                MessageBox.Show("Δεν μπορείτε να εγγραφείτε αν δεν έχετε συμπληρώσει σωστά το email kai to password σας.");
                return;
            }

            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            conn.ConnectionString = Constants.CONNECTION_STRING;

            try
            {
                conn.Open();
                if (validations.ValidEmail(textBoxEmail.Text.ToString()) && validations.ValidPassword(textBoxPassword.Text, textBoxPassword1.Text)){
                    String user = textBoxUser.Text.ToString();
                    String password = textBoxPassword.Text.ToString();
                    String email = textBoxEmail.Text.ToString();
                 


                    OleDbCommand command = new OleDbCommand(@"INSERT INTO [User]( [UserName], [Password], [Email])
                        VALUES(@username, @password, @email)", conn);

                  
                    command.Parameters.AddWithValue("@userName", user);
                    command.Parameters.AddWithValue("@password", password);
                    command.Parameters.AddWithValue("@email", email);
           
                    command.ExecuteNonQuery();

                 

                    MessageBox.Show("Έπιτυχής Εγγραφή");
                    conn.Close();
                    //Σταύρος: Αντι να καλεί καινούριo form θα βάλουμε να καλεί την ηδη ανοιχτη φορμα κάνοντας απλά close.
                    //LoginForm login = new LoginForm();
                    // login.ShowDialog();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Αποτυχιά σύνδεσης λόγω" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            textBoxPassword1.PasswordChar = '*';
            arePasswordsStrongAndNonEmpty();
        }
        

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox5.Visible = true;
            pictureBox4.Visible = false;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox5.Visible = false;
            pictureBox4.Visible = true;
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            textBoxPassword.PasswordChar = '*';
            arePasswordsStrongAndNonEmpty();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            textBoxEmail.Clear();
            textBoxPassword.Clear();
            textBoxPassword1.Clear();
            textBoxUser.Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void arePasswordsStrongAndNonEmpty()
        {
            String password = textBoxPassword.Text;
            String password1 = textBoxPassword1.Text;
            // kalei th function afth gia na:
            // 1) αν ειναι και τα δυο textBox αδεια να γινει ξανα ασπρο.
            // 2) αν ο αλλος βαλει πρωτα το 2ο password και μετα το 1ο, θα αλλαξει σε πρασινο και πάλι.
            if (string.IsNullOrEmpty(password) && string.IsNullOrEmpty(password1))
            {
                textBoxPassword.BackColor = Color.White;
                textBoxPassword1.BackColor = Color.White;
            }
            else if (validations.ValidPassword(password, password1))
            {
                if (isPasswordStrong(textBoxPassword1.Text))
                {
                    textBoxPassword.BackColor = Color.Green;
                    textBoxPassword1.BackColor = Color.Green;
                }
                else
                {
                    MessageBox.Show("Ο κωδικός σας είναι αδύναμος. Ξαναδοκιμάστε");
                    textBoxPassword.Clear();
                    textBoxPassword1.Clear();
                }
            }
            else
            {
                textBoxPassword.BackColor = Color.Red;
                textBoxPassword1.BackColor = Color.Red;
            }
        }

        private void textBoxEmail_TextChanged(object sender, EventArgs e)
        {
            // βάζω το regex tou email
            // για να δω αν ειναι πρασινο
            bool isEmail = Regex.IsMatch(textBoxEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            if (string.IsNullOrEmpty(textBoxEmail.Text))
            {
                textBoxEmail.BackColor = Color.White;
            }
            else if (isEmail)
            {
                textBoxEmail.BackColor = Color.Green;
            }
            else
            {
                textBoxEmail.BackColor = Color.Red;
            }
        }

        private bool isPasswordStrong(String password)
        {
            int score = 0;

            if (password.Length < 1)
                return false;
            if (password.Length < 4)
                return false;
            if (password.Length >= 8)
                score++;
            if (password.Length >= 12)
                score++;
            if (Regex.Match(password, @"\d+", RegexOptions.ECMAScript).Success)
                score++;
            if (Regex.Match(password, @"[a-z]", RegexOptions.ECMAScript).Success &&
              Regex.Match(password, @"[A-Z]", RegexOptions.ECMAScript).Success)
                score++;
            if (Regex.Match(password, @".[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript).Success)
                score++;
            switch (score)
            {
                case 3:
                case 4:
                case 5:
                    return true;
                default:
                case 0:
                case 1:
                case 2:
                    return false;
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void indexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
