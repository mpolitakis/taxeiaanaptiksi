﻿using System;
using WMPLib;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Speech.Synthesis;

namespace TaxeiaAnaptiksi
{
    public partial class CaveForm : Form
    {
        Login userLogin = new Login();
        public CaveForm()
        {
            InitializeComponent();
            userLogin.caveForm = true;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            // ama akouei kati, ftiaxnei analogws tis eikones
            if (SoundClass.doYouHearMusic)
            {
                pictureBox5.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // ama akouei mousikh thn kleinei
            if (SoundClass.doYouHearMusic)
            {
                SoundClass.soundVariable.Stop();
                SoundClass.doYouHearMusic = false;
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
            // ama akouei to synthesizer to kleinei
            if (textToSpeech.speech.State==SynthesizerState.Speaking)
            {
                textToSpeech.speech.Pause();
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
            }
            axWindowsMediaPlayer1.URL = "videos//melissani_cave.mp4";
            axWindowsMediaPlayer1.Ctlcontrols.play();
        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox5.Visible = false;
            pictureBox4.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            // ama milaei to tts kleise to
            if (textToSpeech.speech.State == SynthesizerState.Speaking)
            {
                textToSpeech.speech.Pause();
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
            }
            
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox5.Visible = true;
            pictureBox4.Visible = false;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            // ama akouei th mousikh kai klikarei to text to speech kleinei th mousikh
            if (SoundClass.doYouHearMusic)
            {
                SoundClass.soundVariable.Stop();
                SoundClass.doYouHearMusic = false;
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
            pictureBox1.Visible = true;
            pictureBox2.Visible = false;
            textToSpeech.speech.SpeakAsync(richTextBox1.Text);
            textToSpeech.speech.Resume(); 
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            textToSpeech.speech.Pause();
            pictureBox1.Visible = false;
            pictureBox2.Visible = true;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CaveForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            textToSpeech.speech.SpeakAsyncCancelAll();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private bool checkIfUserInDb()
        {
            if (Validations.isUserLoggedIn == false)
            {
                MessageBox.Show("Πρέπει να είστε εγγεγραμμένος για να έχετε πρόσβαση σε αυτή τη φόρμα.");
                RegisterForm f8 = new RegisterForm();
                f8.ShowDialog();
                return false;
            }
            return true;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void indexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
