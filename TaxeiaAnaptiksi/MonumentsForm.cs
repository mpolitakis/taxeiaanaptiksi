﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxeiaAnaptiksi
{
    public partial class MonumentsForm : Form
    {
        Login userLogin = new Login();
        public MonumentsForm()
        {
            InitializeComponent();
            userLogin.monumentsForm = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form6_Load(object sender, EventArgs e)
        {
            if (SoundClass.doYouHearMusic)
            {
                pictureBox5.Visible = true;
                pictureBox4.Visible = false;
            }
            else
            {
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            // ama milaei to tts kleise to kai alla3e analogws tis eikones
            if (textToSpeech.speech.State == SynthesizerState.Speaking)
            {
                textToSpeech.speech.Pause();
                pictureBox6.Visible = false;
                pictureBox7.Visible = true;
                pictureBox8.Visible = false;
                pictureBox9.Visible = true;
            }
            SoundClass.soundVariable.Play();
            SoundClass.doYouHearMusic = true;
            pictureBox5.Visible = true;
            pictureBox4.Visible = false;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            SoundClass.soundVariable.Stop();
            SoundClass.doYouHearMusic = false;
            pictureBox5.Visible = false;
            pictureBox4.Visible = true;
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            // ama akouei th mousikh kai klikarei to text to speech kleinei th mousikh
            if (SoundClass.doYouHearMusic)
            {
                SoundClass.soundVariable.Stop();
                SoundClass.doYouHearMusic = false;
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
            // afto gia na stamathsei to allo tts pou exei sthn selida an paizei.
            if (textToSpeech.speech.State==SynthesizerState.Speaking)
            {
                textToSpeech.speech.SpeakAsyncCancelAll();
                pictureBox8.Visible = false;
                pictureBox9.Visible = true;
            }
            pictureBox6.Visible = true;
            pictureBox7.Visible = false;
            textToSpeech.speech.SpeakAsync(richTextBox3.Text);
            textToSpeech.speech.Resume();
        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            textToSpeech.speech.Pause();
            pictureBox6.Visible = false;
            pictureBox7.Visible = true;
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            // ama akouei th mousikh kai klikarei to text to speech kleinei th mousikh
            if (SoundClass.doYouHearMusic)
            {
                SoundClass.soundVariable.Stop();
                SoundClass.doYouHearMusic = false;
                pictureBox5.Visible = false;
                pictureBox4.Visible = true;
            }
            // afto gia na stamathsei to allo tts pou exei sthn selida an paizei.
            if (textToSpeech.speech.State == SynthesizerState.Speaking)
            {
                textToSpeech.speech.SpeakAsyncCancelAll();
                pictureBox6.Visible = false;
                pictureBox7.Visible = true;
            }
            // afto gia na stamathsei to allo tts pou exei sthn selida an paizei.
            textToSpeech.speech.SpeakAsyncCancelAll();
            pictureBox8.Visible = true;
            pictureBox9.Visible = false;
            textToSpeech.speech.SpeakAsync(richTextBox1.Text);
            textToSpeech.speech.Resume();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            textToSpeech.speech.Pause();
            pictureBox8.Visible = false;
            pictureBox9.Visible = true;
        }

        private void MonumentsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            textToSpeech.speech.SpeakAsyncCancelAll();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void εγγραφήΧρήστηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegisterForm f8 = new RegisterForm();
            f8.ShowDialog();
        }

        private void καταδύσειςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                ScubaDivingForm f9 = new ScubaDivingForm();
                f9.ShowDialog();
            }
        }
        private bool checkIfUserInDb()
        {
            if (Validations.isUserLoggedIn == false)
            {
                MessageBox.Show("Πρέπει να είστε εγγεγραμμένος για να έχετε πρόσβαση σε αυτή τη φόρμα.");
                RegisterForm f8 = new RegisterForm();
                f8.ShowDialog();
                return false;
            }
            return true;
        }

        private void μνημείαToolStripMenuItem_Click(object sender, EventArgs e)
        {
                MessageBox.Show("Είναι ήδη ανοιχτή αυτή η σελίδα.");
        }

        private void παραλίεςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                BeachesForm f11 = new BeachesForm();
                f11.ShowDialog();
            }
        }

        private void σπηλιάToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (checkIfUserInDb())
            {
                CaveForm f12 = new CaveForm();
                f12.ShowDialog();
            }
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Είναι ήδη ανοιχτή αυτή η σελίδα. Πηγαίντε δύο φορές πίσω.");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Stavros Peppas and Michael Politakis!");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void calendarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CalendarForm f1 = new CalendarForm();
            f1.ShowDialog();
        }

        private void indexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"..\..\kefalonia.chm");
        }
    }
}
